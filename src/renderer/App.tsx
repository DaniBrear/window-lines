import { MemoryRouter as Router, Routes, Route } from 'react-router-dom';
import './App.css';
import { useEffect, useState } from 'react';

function Hello() {
  const [clientX, setClientX] = useState(0);
  const [clientY, setClientY] = useState(0);

  const [show4, setShow4] = useState(true);
  const [show3, setShow3] = useState(true);
  const [show2, setShow2] = useState(true);

  useEffect(() => {
    const listener = (e: { clientX: number; clientY: number }) => {
      setClientX(e.clientX);
      setClientY(e.clientY);
    };
    window.addEventListener('mousemove', listener);
    return () => {
      window.removeEventListener('mousemove', listener);
    };
  }, []);

  return (
    <div>
      {show3 && (
        <div className="h-line h-1-3">
          <p style={{ left: clientX }}>1/3</p>
        </div>
      )}
      {(show2 || show4) && (
        <div className="h-line h-1-2">
          <p style={{ left: clientX }}>{show2 ? '1/2' : '2/4'}</p>
        </div>
      )}
      {show3 && (
        <div className="h-line h-2-3">
          <p style={{ left: clientX }}>2/3</p>
        </div>
      )}

      {show4 && (
        <div className="v-line v-1-4">
          <p style={{ top: clientY }}>1/4</p>
        </div>
      )}
      {show3 && (
        <div className="v-line v-1-3">
          <p style={{ top: clientY }}>1/3</p>
        </div>
      )}
      {(show2 || show4) && (
        <div className="v-line v-1-2">
          <p style={{ top: clientY }}>{show2 ? '1/2' : '2/4'}</p>
        </div>
      )}
      {show3 && (
        <div className="v-line v-2-3">
          <p style={{ top: clientY }}>2/3</p>
        </div>
      )}
      {show4 && (
        <div className="v-line v-3-4">
          <p style={{ top: clientY }}>3/4</p>
        </div>
      )}

      <div
        className="show-boxes"
        style={{ opacity: clientX < 300 && clientY < 300 ? 1 : 0.5 }}
      >
        <label htmlFor="check-1-4">
          <input
            id="check-1-4"
            type="checkbox"
            checked={show4}
            onChange={(e) => setShow4(e.target.checked)}
          />{' '}
          1/4
        </label>
        <label htmlFor="check-1-3">
          <input
            id="check-1-3"
            type="checkbox"
            checked={show3}
            onChange={(e) => setShow3(e.target.checked)}
          />{' '}
          1/3
        </label>
        <label htmlFor="check-1-2">
          <input
            id="check-1-2"
            type="checkbox"
            checked={show2}
            onChange={(e) => setShow2(e.target.checked)}
          />{' '}
          1/2
        </label>
      </div>
    </div>
  );
}

export default function App() {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<Hello />} />
      </Routes>
    </Router>
  );
}
